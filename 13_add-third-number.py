if __name__ == "__main__":

    #taking input from user in string form and converting it to list using split()
    input = input().split()

    #initialising the output list to empty
    output = []

    #keeping a counter for index set to 3 which picks the third element
    item_index=2

    while (len(input)!=0):
        output.append(input[item_index])                    #appending the element at item_index to output list
        del input[item_index]                               #UPDATED input list by deleting item at item_index

        #Note that we've now the item_index already pointing to next element

        if((len(input)-1)-item_index>=2):
            item_index+=2                                   #as item_index already points to next item we add only 2 to get next item_index
        elif(len(input)>2):                                 #to check if list needs to be checked in circular manner
            item_index=(2-((len(input)-1)-item_index))-1    #subtracting the residual of last index - item_index from 2 and finally decrementing to get index of that element
        elif(len(input)==2):                                #whenever we have 2 elements in a list, we know traversing 3 times in circular list gives the same element so we pass
            pass
        else:                                               #in a single element array the only element left is at index 0
            item_index=0
    print(output)
